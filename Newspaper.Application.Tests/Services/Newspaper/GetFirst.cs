﻿using System;
using System.Threading.Tasks;
using Moq;
using Newspaper.Application.Tests.Framework.Services.Newspaper.Base;
using Newspaper.Domain.Models;
using NUnit.Framework;

namespace Newspaper.Application.Tests.Framework.Services.Newspaper
{
    [TestFixture]
    public class GetFirst : NewspaperBaseTest
    {
        [Test]
        public async Task NotEmpty()
        {
            ArticleView result = await NewspaperService.GetFirstAsync(It.IsAny<string>());

            Assert.IsNotNull(result);
        }

        [Test]
        public async Task NotEmptyFields()
        {
            ArticleView result = await NewspaperService.GetFirstAsync(It.IsAny<string>());

            Assert.IsNotNull(result.Heading);
            Assert.IsNotNull(result.Link);
            Assert.IsNotNull(result.Updated);
        }

        [Test]
        public async Task Exact()
        {
            ArticleView result = await NewspaperService.GetFirstAsync(It.IsAny<string>());

            Assert.AreEqual("Coronavirus Pandemic Pulls Millions Back Into Poverty: Live Coverage", result.Heading);
            Assert.AreEqual("https://nyti.ms/2WpBNqj", result.Link);
            Assert.AreEqual(new DateTime(2020, 4, 30).ToShortDateString(), result.Updated.ToShortDateString());
        }

        [Test]
        public async Task RemoteApiCalledOnce()
        {
            await NewspaperService.GetFirstAsync(It.IsAny<string>());

            HttpClientHandlerMock.Verify(client => client.GetAsync(It.IsAny<string>()), Times.Once());
        }
    }
}