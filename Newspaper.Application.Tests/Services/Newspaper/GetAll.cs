﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Moq;
using Newspaper.Application.Tests.Framework.Services.Newspaper.Base;
using Newspaper.Domain.Models;
using NUnit.Framework;

namespace Newspaper.Application.Tests.Framework.Services.Newspaper
{
    [TestFixture]
    public class GetAll : NewspaperBaseTest
    {
        [Test]
        public async Task NotEmpty()
        {
            List<ArticleView> result = await NewspaperService.GetAllAsync(It.IsAny<string>());

            Assert.IsNotEmpty(result);
        }

        [Test]
        public async Task ExactCount()
        {
            List<ArticleView> result = await NewspaperService.GetAllAsync(It.IsAny<string>());

            Assert.AreEqual(39, result.Count);
        }

        [Test]
        public async Task ExactCount_Negative()
        {
            List<ArticleView> result = await NewspaperService.GetAllAsync(It.IsAny<string>());

            Assert.AreNotEqual(25, result.Count);
        }

        [Test]
        public async Task RemoteApiCalledOnce()
        {
            await NewspaperService.GetAllAsync(It.IsAny<string>());

            HttpClientHandlerMock.Verify(client => client.GetAsync(It.IsAny<string>()), Times.Once());
        }
    }
}