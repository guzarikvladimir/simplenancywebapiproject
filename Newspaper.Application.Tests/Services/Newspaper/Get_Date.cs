﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Moq;
using Newspaper.Application.Tests.Framework.Services.Newspaper.Base;
using Newspaper.Domain.Models;
using NUnit.Framework;

namespace Newspaper.Application.Tests.Framework.Services.Newspaper
{
    [TestFixture]
    public class Get_Date : NewspaperBaseTest
    {
        private readonly DateTime _date = new DateTime(2020, 4, 29);

        [Test]
        public async Task NotEmpty()
        {
            List<ArticleView> result = await NewspaperService.GetAsync(It.IsAny<string>(), _date);

            Assert.IsNotEmpty(result);
        }

        [Test]
        public async Task NotEmpty_Negative()
        {
            List<ArticleView> result = await NewspaperService.GetAsync(It.IsAny<string>(), DateTime.MinValue);

            Assert.IsEmpty(result);
        }

        [Test]
        public async Task ExactCount()
        {
            List<ArticleView> result = await NewspaperService.GetAsync(It.IsAny<string>(), _date);

            Assert.AreEqual(9, result.Count);
        }

        [Test]
        public async Task ExactCount_Negative()
        {
            List<ArticleView> result = await NewspaperService.GetAsync(It.IsAny<string>(), _date);

            Assert.AreNotEqual(15, result.Count);
        }

        [Test]
        public async Task RemoteApiCalledOnce()
        {
            await NewspaperService.GetAsync(It.IsAny<string>(), _date);

            HttpClientHandlerMock.Verify(client => client.GetAsync(It.IsAny<string>()), Times.Once());
        }
    }
}