﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Moq;
using Newspaper.Application.Services.Newspaper;
using Newspaper.Application.Tests.Properties;
using Newspaper.Domain.Contract;
using Newspaper.Infrastructure.RemoteServices.HttpClient;
using Newspaper.Infrastructure.RemoteServices.Impl;
using NUnit.Framework;

namespace Newspaper.Application.Tests.Framework.Services.Newspaper.Base
{
    public class NewspaperBaseTest
    {
        protected INewspaperRetrievingService NewspaperService;
        protected Mock<IHttpClientHandler> HttpClientHandlerMock;

        [SetUp]
        public void SetUp()
        {
            HttpClientHandlerMock = new Mock<IHttpClientHandler>();
            HttpClientHandlerMock.Setup(client => client.GetAsync(It.IsAny<string>()))
                .Returns(Task.FromResult(new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(Resources.NewspaperContent)
                }));

            INewsApi newsApi = new NewsApi(HttpClientHandlerMock.Object);
            NewspaperService = new NewspaperRetrievingService(newsApi);
        }
    }
}