﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Moq;
using Newspaper.Application.Tests.Framework.Services.Newspaper.Base;
using Newspaper.Domain.Models;
using NUnit.Framework;

namespace Newspaper.Application.Tests.Framework.Services.Newspaper
{
    [TestFixture]
    public class GetGroupedByDate : NewspaperBaseTest
    {
        [Test]
        public async Task NotEmpty()
        {
            List<ArticleGroupByDateView> result = await NewspaperService.GetGroupedByDateAsync(It.IsAny<string>());

            Assert.IsNotEmpty(result);
        }

        [Test]
        public async Task ExactCount()
        {
            List<ArticleGroupByDateView> result = await NewspaperService.GetGroupedByDateAsync(It.IsAny<string>());

            Assert.AreEqual(2, result.Count);
        }

        [Test]
        public async Task ExactCount_Negative()
        {
            List<ArticleGroupByDateView> result = await NewspaperService.GetGroupedByDateAsync(It.IsAny<string>());

            Assert.AreNotEqual(5, result.Count);
        }

        [Test]
        public async Task Exact()
        {
            List<ArticleGroupByDateView> result = await NewspaperService.GetGroupedByDateAsync(It.IsAny<string>());

            Assert.AreEqual(new DateTime(2020, 4, 30).ToShortDateString(), result.First().Date);
            Assert.AreEqual(30, result.First().Total);
        }

        [Test]
        public async Task RemoteApiCalledOnce()
        {
            await NewspaperService.GetGroupedByDateAsync(It.IsAny<string>());

            HttpClientHandlerMock.Verify(client => client.GetAsync(It.IsAny<string>()), Times.Once());
        }
    }
}