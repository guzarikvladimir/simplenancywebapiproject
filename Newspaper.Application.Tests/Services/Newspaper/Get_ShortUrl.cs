﻿using System.Threading.Tasks;
using Moq;
using Newspaper.Application.Tests.Framework.Services.Newspaper.Base;
using Newspaper.Domain.Models;
using NUnit.Framework;

namespace Newspaper.Application.Tests.Framework.Services.Newspaper
{
    [TestFixture]
    public class Get_ShortUrl : NewspaperBaseTest
    {
        [Test]
        public async Task NotEmpty()
        {
            ArticleView result = await NewspaperService.GetAsync("3bMK1z0");

            Assert.IsNotNull(result);
        }

        [Test]
        public async Task NotEmpty_Negative()
        {
            ArticleView result = await NewspaperService.GetAsync("urlthatdoesnotexist");

            Assert.IsNull(result);
        }

        [Test]
        public async Task Exact()
        {
            ArticleView result = await NewspaperService.GetAsync("3bMK1z0");

            Assert.AreEqual("Bollywood Reels From Deaths of 2 Stars in 2 Days", result.Heading);
            Assert.AreEqual("https://nyti.ms/3bMK1z0", result.Link);
            Assert.IsNotNull(result.Updated);
        }

        [Test]
        public async Task RemoteApiCalledOnce()
        {
            await NewspaperService.GetAsync("3bMK1z0");

            HttpClientHandlerMock.Verify(client => client.GetAsync(It.IsAny<string>()), Times.Once());
        }
    }
}