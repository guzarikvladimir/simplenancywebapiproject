﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newspaper.Domain.Models;

namespace Newspaper.Application.Services.Newspaper
{
    public interface INewspaperRetrievingService
    {
        Task<List<ArticleView>> GetAllAsync(string sectionName);

        Task<ArticleView> GetFirstAsync(string sectionName);

        Task<List<ArticleView>> GetAsync(string sectionName, DateTime date);

        Task<ArticleView> GetAsync(string shortUrl);

        Task<List<ArticleGroupByDateView>> GetGroupedByDateAsync(string sectionName);
    }
}