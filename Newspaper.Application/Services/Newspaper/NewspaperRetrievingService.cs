﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newspaper.Domain.Contract;
using Newspaper.Domain.Models;

namespace Newspaper.Application.Services.Newspaper
{
    public class NewspaperRetrievingService : INewspaperRetrievingService
    {
        private readonly INewsApi _newsApi;

        public NewspaperRetrievingService(INewsApi newsApi)
        {
            _newsApi = newsApi;
        }

        public async Task<List<ArticleView>> GetAllAsync(string sectionName)
        {
            List<ArticleView> items = await _newsApi.GetAllAsync(sectionName);

            return items;
        }

        public async Task<ArticleView> GetFirstAsync(string sectionName)
        {
            return (await GetAllAsync(sectionName)).FirstOrDefault();
        }

        public async Task<List<ArticleView>> GetAsync(string sectionName, DateTime date)
        {
            string shortDate = date.ToShortDateString();
            return (await GetAllAsync(sectionName))
                .Where(a => a.Updated.ToShortDateString() == shortDate)
                .ToList();
        }

        public async Task<ArticleView> GetAsync(string shortUrl)
        {
            return (await _newsApi.GetAllAsync("home"))
                .FirstOrDefault(a => a.Link.EndsWith(shortUrl));
        }

        public async Task<List<ArticleGroupByDateView>> GetGroupedByDateAsync(string sectionName)
        {
            return (await GetAllAsync(sectionName))
                .GroupBy(a => a.Updated.ToShortDateString())
                .Select(g => new ArticleGroupByDateView()
                {
                    Date = g.Key,
                    Total = g.Count()
                })
                .ToList();
        }
    }
}