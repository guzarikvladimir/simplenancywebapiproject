﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Newspaper.Infrastructure.RemoteServices.HttpClient
{
    public interface IHttpClientHandler
    {
        Task<HttpResponseMessage> GetAsync(string url);

        Task<HttpResponseMessage> PostAsync(string url, HttpContent content);
    }
}