﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Newspaper.Infrastructure.RemoteServices.HttpClient
{
    public class HttpClientHandler : IHttpClientHandler
    {
        private readonly System.Net.Http.HttpClient _client;

        public HttpClientHandler(System.Net.Http.HttpClient client)
        {
            _client = client;
        }

        public async Task<HttpResponseMessage> GetAsync(string url)
        {
            return await _client.GetAsync(url);
        }

        public async Task<HttpResponseMessage> PostAsync(string url, HttpContent content)
        {
            return await _client.PostAsync(url, content);
        }
    }
}