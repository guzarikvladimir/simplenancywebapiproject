﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newspaper.Domain.Contract;
using Newspaper.Domain.Models;
using Newspaper.Infrastructure.RemoteServices.HttpClient;
using Newspaper.Infrastructure.RemoteServices.Models;

namespace Newspaper.Infrastructure.RemoteServices.Impl
{
    public class NewsApi : RemoteApiBase, INewsApi
    {
        private const string BaseAddress = "https://api.nytimes.com/svc/topstories/v2/";
        private const string ApiKey = "k0XA0k0jJGAVuv8Jr5wAIcKDGPuznmRJ";

        public NewsApi(IHttpClientHandler httpClient) : base(httpClient)
        {
        }

        public async Task<List<ArticleView>> GetAllAsync(string sectionName)
        {
            var response = await MakeGetRequest<NewsResponse>($"{BaseAddress}{sectionName}.json?api-key={ApiKey}");

            return response.Results
                .Select(item => new ArticleView()
                {
                    Heading = item.Title,
                    Link = item.Short_url,
                    Updated = item.Updated_date
                })
                .ToList();
        }
    }
}