﻿using System.Net.Http;
using System.Threading.Tasks;
using Newspaper.Infrastructure.RemoteServices.HttpClient;
using Newtonsoft.Json;

namespace Newspaper.Infrastructure.RemoteServices
{
    public abstract class RemoteApiBase
    {
        protected readonly IHttpClientHandler HttpClient;

        protected RemoteApiBase(IHttpClientHandler httpClient)
        {
            HttpClient = httpClient;
        }

        protected async Task<TResponse> MakeGetRequest<TResponse>(string url)
        {
            HttpResponseMessage response = await HttpClient.GetAsync(url);
            string rawData = await response.Content.ReadAsStringAsync();
            var data = JsonConvert.DeserializeObject<TResponse>(rawData);

            return data;
        }
    }
}