﻿using System.Collections.Generic;

namespace Newspaper.Infrastructure.RemoteServices.Models
{
    public class NewsResponse
    {
        public List<ResultItem> Results { get; set; }
    }
}