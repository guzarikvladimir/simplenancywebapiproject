﻿using System;

namespace Newspaper.Infrastructure.RemoteServices.Models
{
    public class ResultItem
    {
        public string Title { get; set; }

        public DateTime Updated_date { get; set; }

        public string Short_url { get; set; }
    }
}