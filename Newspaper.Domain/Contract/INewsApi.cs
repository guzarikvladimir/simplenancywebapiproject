﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Newspaper.Domain.Models;

namespace Newspaper.Domain.Contract
{
    public interface INewsApi
    {
        Task<List<ArticleView>> GetAllAsync(string sectionName);
    }
}