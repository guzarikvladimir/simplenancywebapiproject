﻿namespace Newspaper.Domain.Models
{
    public class ArticleGroupByDateView
    {
        public string Date { get; set; }

        public int Total { get; set; }
    }
}