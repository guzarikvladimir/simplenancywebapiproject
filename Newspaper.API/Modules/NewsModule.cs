﻿using System.Text.Json;
using Nancy;
using Newspaper.Application.Services.Newspaper;

namespace Newspaper.API.Modules
{
    public sealed class NewsModule : NancyModule
    {
        public NewsModule(INewspaperRetrievingService newspaperRetrievingService)
        {
            Get("list/{section}", async args =>
            {
                var result = await newspaperRetrievingService.GetAllAsync(args.section);
                return JsonSerializer.Serialize(result);
            });
            Get("/list/{section}/first", async args =>
            {
                var result = await newspaperRetrievingService.GetFirstAsync(args.section);
                return JsonSerializer.Serialize(result);
            });
            Get("/list/{section}/{updatedDate}", async args =>
            {
                var result = await newspaperRetrievingService.GetAsync(args.section, args.updatedDate);
                return JsonSerializer.Serialize(result);
            });
            Get("/article/{shortUrl}", async args =>
            {
                var result = await newspaperRetrievingService.GetAsync(args.shortUrl);
                return JsonSerializer.Serialize(result);
            });
            Get("/group/{section}", async args =>
            {
                var result = await newspaperRetrievingService.GetGroupedByDateAsync(args.section);
                return JsonSerializer.Serialize(result);
            });
        }
    }
}