﻿using Nancy;

namespace Newspaper.API.Modules
{
    public sealed class HomeModule : NancyModule
    {
        public HomeModule()
        {
            Get("/", _ => "Ok");
        }
    }
}