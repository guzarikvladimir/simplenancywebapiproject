﻿using System;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.Responses.Negotiation;
using Nancy.TinyIoc;
using Newspaper.Application.Services.Newspaper;
using Newspaper.Domain.Contract;
using Newspaper.Infrastructure.RemoteServices.HttpClient;
using Newspaper.Infrastructure.RemoteServices.Impl;

namespace Newspaper.API.DependencyInjection
{
    public class CustomBootstraper : DefaultNancyBootstrapper
    {
        protected override void ConfigureRequestContainer(
            TinyIoCContainer container, 
            NancyContext context)
        {
            container.Register<INewsApi, NewsApi>();
            container.Register<INewspaperRetrievingService, NewspaperRetrievingService>();
            container.Register<IHttpClientHandler, HttpClientHandler>();
        }

        //protected override Func<ITypeCatalog, NancyInternalConfiguration> InternalConfiguration
        //{
        //    get
        //    {
        //        return NancyInternalConfiguration.WithOverrides(
        //            (c) =>
        //            {
        //                c.ResponseProcessors.Clear();
        //                c.ResponseProcessors.Add(typeof(JsonProcessor));
        //            });
        //    }
        //}
    }
}